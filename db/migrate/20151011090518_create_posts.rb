class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string  :body,  default: ""
      t.string  :title
      t.integer :user_id

      t.timestamps
    end
  end
end
