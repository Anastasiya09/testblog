class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string  :body
      t.integer :parent_id
      t.integer :user_id
      t.integer :post_id

      t.timestamps
    end

    add_index "comments", ["user_id"], name: "user_id", using: :btree
    add_index "comments", ["post_id"], name: "post_id", using: :btree
  end
end
