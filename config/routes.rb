Rails.application.routes.draw do
  devise_for :users

  resources :comments, only: [:new, :create, :destroy] do
    collection do
      match :list_comments, via: [:get, :post]
      get :reply_comments
    end
  end

  resources :posts
  resources :profiles

  root to: 'posts#index'
end
