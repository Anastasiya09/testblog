#this file is configuration file for declarative_authirization

authorization do

  role :anonymous do
    has_permission_on [:posts], to: [:index, :show]
    has_permission_on [:profiles], to: [:show]
    has_permission_on [:comments], to: [:list_comments]
  end

  role :user do
    includes :anonymous
    has_permission_on [:posts], to: [:new, :create]
    has_permission_on :posts, to: [:edit, :update, :destroy] do
      if_attribute user_id: is {user.id}
    end
    has_permission_on [:comments], to: [:create, :reply_comments]
    has_permission_on :comments, to: [:destroy] do
      if_attribute user_id: is {user.id}
    end
    has_permission_on [:profiles], to: [:edit, :update]
  end

  role :admin do
    includes :user
    includes :newsmaker
  end
end