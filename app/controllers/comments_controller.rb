class CommentsController < ApplicationController
  filter_access_to
  before_action :load_comment, only: [:edit, :reply_comments, :destroy]

  def create
    @comment = Comment.new(comment_params)
    @comment.user = current_user
    @post = @comment.post
    @comment.save
  end

  def destroy
    @post = @comment.post
    @comment.destroy
  end

  def list_comments
    all_comments = Post.find(params[:post_id]).comments.includes(:user)
    @comments = all_comments.group_by(&:parent_id)
    render layout: false
  end

  def reply_comments
    @post = @comment.post
    render layout: false
  end

  private
    def load_comment
      @comment = Comment.find_by_id params[:id]
    end

    def comment_params
      params.require(:comment).permit(:body, :post_id, :parent_id)
    end
end
