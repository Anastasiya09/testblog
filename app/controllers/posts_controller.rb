class PostsController < ApplicationController

  filter_access_to
  before_action :find_resource!, only: [:show, :edit, :destroy, :create,
                                        :update]
  before_action :build_resource, only: [:new, :create]

  def index
    @posts = Post.all.includes(:user).paginate page: calc_page(params[:page], 10, Post.all.count), per_page: 10
  end

  def show
  end

  def new
  end

  def create
    if @post.prepare_and_save_new_post(current_user)
      redirect_to post_path(@post)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @post.update(post_params)
      redirect_to post_path(@post)
    else
      render :edit
    end
  end

  def destroy
    @post.destroy
    redirect_to root_path
  end

  private 
    def find_resource!
      post_id = params[:id]
      @post = Post.find_by_id(post_id)
    end

    def post_params
      params.require(:post).permit( :title, :body) if params[:post]
    end

    def build_resource
      @post = Post.new(post_params)
    end
end
