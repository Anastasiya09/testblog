class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_current_user

  def calc_page page, per_page, count
    if page.to_i * per_page.to_i > count
      (count.to_f / per_page.to_i).ceil
    else
      page
    end
  end

  protected
    def set_current_user
      Authorization.current_user = current_user
    end
end
