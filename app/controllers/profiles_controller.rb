class ProfilesController < ApplicationController
  filter_access_to
  before_action :find_user, only: [:show, :edit, :update]

  def show
    @posts = @profile.posts.paginate page: calc_page(params[:page], 10, @profile.posts.count), per_page: 10
  end

  def edit
  end

  def update
    binding.pry
    if @profile.update(profile_params)
      sign_in User, @profile, bypass: true
      redirect_to profile_path
    else
     render :edit
    end 
  end

  private
    def find_user
      @profile = User.find(params[:id])
    end

    def profile_params
      params.require(:user).permit(:email, :password, :password_confirmation, :avatar)
    end
end
