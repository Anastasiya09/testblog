class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many   :posts
  has_many   :comments

  has_attached_file :avatar,
    medium: {
      geometry: "30x30#",
      format: :png,
      paperclip_optimizer: {
        gifsicle: false,
        pngquant: false,
        jhead: false,
        jpegoptim: false,
        svgo: false,
        pngout: {strategy: 0},
        advpng: {level: 4},
        optipng: {level: 7},
        pngcrush: {chunks: 'alla'}
      }
    },
  path: "public/system/users/:id/avatars/:style.:extension",
  url: "/system/users/:id/avatars/:style.:extension",
  default_url: "/images/userpic-default.png"

  validates_attachment_content_type :avatar, :content_type => %w(image/jpeg image/jpg image/png)

  def role_symbols#Используется в declarative_authorization
    roles = role.split(',')
    roles.map do |role|
      role.to_sym
    end
  end
end
