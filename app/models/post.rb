class Post < ActiveRecord::Base
  belongs_to :user
  has_many   :comments, dependent: :destroy

  def prepare_and_save_new_post(current_user)
    self.user = current_user
    if save
      return true
    end
  end
end
