class Comment < ActiveRecord::Base
  belongs_to :parent, class_name: 'Comment'
  belongs_to :post
  belongs_to :user

  before_destroy :destroy_children

  def has_child?
    Comment.count( conditions: { parent_id: id } ) > 0
  end

  def destroy_children
    Comment.where(parent_id: id).destroy_all
  end
end
