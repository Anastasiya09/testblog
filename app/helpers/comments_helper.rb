module CommentsHelper
  def comments_count post
    post.comments.count
  end
end
