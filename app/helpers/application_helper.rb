module ApplicationHelper
  def link_to_user(user, link_class = nil)
    link_to(user.email, profile_url(user), class: link_class)
  end
end
