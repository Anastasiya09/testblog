function key_ctrl_enter(){
  $('.reply').keydown(function (e)
  {
      if (e.ctrlKey && e.keyCode == 13) {
          $(this).submit();
          $(this).find('textarea#comment').val('');
      }
  });
}

function click_reply(){
  $('a.reply').on( 'click', function(e)
  {
    var event = jQuery.Event("keydown", { keyCode: 27 });
    jQuery(".reply").trigger( event );
    var _this = e.target;
    $.ajax({
      url: "/comments/reply_comments?id=" + _this.id,
      success: function(data) {
        $(_this).after(data)
        key_ctrl_enter();
        $(_this).css('display', 'none');
        $('.reply').keydown(function (e)
        {
            if (e.keyCode == 27) {
                $('.box form.reply').remove();
                $("a.reply").css('display', 'block');
            }
        });
      }
    });
  });
}